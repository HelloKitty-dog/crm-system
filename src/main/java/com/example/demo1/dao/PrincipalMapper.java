package com.example.demo1.dao;

import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Principal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PrincipalMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Principal record);

    int insertSelective(Principal record);

    Principal selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Principal record);

    int updateByPrimaryKey(Principal record);

    List<Principal> selectAll();

}