package com.example.demo1.dao;

import com.example.demo1.domain.Predict;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PredictMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Predict record);

    int insertSelective(Predict record);

    Predict selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Predict record);

    int updateByPrimaryKey(Predict record);
}