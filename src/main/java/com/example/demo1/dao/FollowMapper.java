package com.example.demo1.dao;

import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Follow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface FollowMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Follow record);

    int insertSelective(Follow record);

    Follow selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Follow record);

    int updateByPrimaryKey(Follow record);

    List<Follow> selectAll();

}