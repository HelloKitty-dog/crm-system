package com.example.demo1.dao;

import com.example.demo1.predicted.Point;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PointMapper {
    /**
     * 获取已知点
     *
     */
    @Select("SELECT * FROM predict")
    List<Point> allPoints();
}
