package com.example.demo1.dao;

import com.example.demo1.domain.Contract;
import com.example.demo1.domain.HighSea;

import java.util.List;

public interface HighSeaMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(HighSea record);

    int insertSelective(HighSea record);

    HighSea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HighSea record);

    int updateByPrimaryKey(HighSea record);
    List<HighSea> selectAll();
}