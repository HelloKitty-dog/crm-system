package com.example.demo1.dao;

import com.example.demo1.domain.MUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MUserMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(MUser record);

    int insertSelective(MUser record);

    MUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MUser record);

    int updateByPrimaryKey(MUser record);

    MUser selectByName(String name);
}