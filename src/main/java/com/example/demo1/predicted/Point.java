package com.example.demo1.predicted;

public class Point {
    private int id;
    private double income;
    private int personnum;
    private String product;

    public Point(int id, double income, int personnum, String product) {
        this.id = id;
        this.income = income;
        this.personnum = personnum;
        this.product = product;
    }

    public Point() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public int getPersonnum() {
        return personnum;
    }

    public void setPersonnum(int personnum) {
        this.personnum = personnum;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}