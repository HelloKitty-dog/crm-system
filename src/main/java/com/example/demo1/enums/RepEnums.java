package com.example.demo1.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  RepEnums {

    SUCCESS(0, "成功"),
    FAILURE(1, "失败"),
    BAS_REQUEST(2, "参数有误"),
    NO_AUTH(3, "无权访问");

    private int code;
    private String msg;

}