package com.example.demo1.enums;


import lombok.Getter;

@Getter
public enum Userstatus {

    AVAILABLE(1,"可用的"),

    ABANDON(2,"已废弃"),

    REGISTERING(3,"注册中");


    private int code;
    private String value;

    private Userstatus(int code,String value) {
        this.code = code;
        this.value = value;
    }


}
