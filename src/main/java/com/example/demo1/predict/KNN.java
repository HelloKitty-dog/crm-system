package com.example.demo1.predict;

import java.util.*;

public class KNN {
    public static void main(String[] args) {

        // 一、输入所有已知点
        List<Point> dataList = creatDataSet();
        // 二、输入未知点
        Point x = new Point(40, 1, 2);
        // 三、计算所有已知点到未知点的欧式距离，并根据距离对所有已知点排序
        CompareClass compare = new CompareClass();
        Set<Distance> distanceSet = new TreeSet<Distance>(compare);
        for (Point point : dataList) {
            distanceSet.add(new Distance(point.getId(), x.getId(), oudistance(point,
                    x)));
        }
        // 四、选取最近的k个点
        double k = 15;

        /**
         * 五、计算k个点所在分类出现的频率
         */
        // 1、计算每个分类所包含的点的个数
        List<Distance> distanceList= new ArrayList<Distance>(distanceSet);
        Map<String, Integer> map = getNumberOfType(distanceList, dataList, k);

        // 2、计算频率
        Map<String, Double> p = computeP(map, k);

        x.setType(maxP(p));
        System.out.println("未知点的类型为："+x.getType());
    }

    // 欧式距离计算
    public static double oudistance(Point point1, Point point2) {
        double temp = Math.pow(point1.getX() - point2.getX(), 2)
                + Math.pow(point1.getY() - point2.getY(), 2);
        return Math.sqrt(temp);
    }

    // 找出最大频率
    public static String maxP(Map<String, Double> map) {
        String key = null;
        double value = 0.0;
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (entry.getValue() > value) {
                key = entry.getKey();
                value = entry.getValue();
            }
        }
        return key;
    }

    // 计算频率
    public static Map<String, Double> computeP(Map<String, Integer> map,
                                               double k) {
        Map<String, Double> p = new HashMap<String, Double>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            p.put(entry.getKey(), entry.getValue() / k);
        }
        return p;
    }

    // 计算每个分类包含的点的个数
    public static Map<String, Integer> getNumberOfType(
            List<Distance> listDistance, List<Point> listPoint, double k) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        int i = 0;
        System.out.println("选取的k个点，由近及远依次为：");
        for (Distance distance : listDistance) {
            System.out.println("id为" + distance.getId() + ",距离为："
                    + distance.getDisatance());
            long id = distance.getId();
            // 通过id找到所属类型,并存储到HashMap中
            for (Point point : listPoint) {
                if (point.getId() == id) {
                    if (map.get(point.getType()) != null)
                        map.put(point.getType(), map.get(point.getType()) + 1);
                    else {
                        map.put(point.getType(), 1);
                    }
                }
            }
            i++;
            if (i >= k)
                break;
        }
        return map;
    }

    public static ArrayList<Point> creatDataSet(){

        Point point1 = new Point(1, 8.8, 3, "A");
        Point point2 = new Point(2, 7.9, 3, "A");
        Point point3 = new Point(3, 8.6, 2, "A");
        Point point4 = new Point(4, 12, 3, "A");
        Point point5 = new Point(5, 11.5, 3, "A");
        Point point6 = new Point(6, 9, 3, "A");
        Point point7 = new Point(7, 13, 2, "A");
        Point point8 = new Point(8, 8, 2, "A");
        Point point9 = new Point(9, 10, 3, "A");
        Point point10 = new Point(10, 8.5, 3, "A");
        Point point11 = new Point(11, 11, 3, "A");
        Point point12 = new Point(12, 8, 3, "A");
        Point point13 = new Point(13, 8.4, 3, "A");
        Point point14 = new Point(14, 13, 3, "A");
        Point point15 = new Point(15, 9.5, 3, "A");

        Point point16 = new Point(16, 52, 2, "B");
        Point point17 = new Point(17, 85, 2, "B");
        Point point18 = new Point(18, 42, 1, "B");
        Point point19 = new Point(19, 60, 1, "B");
        Point point20 = new Point(20, 80, 2, "B");

        Point point21 = new Point(21, 16, 4, "C");
        Point point22 = new Point(22, 12, 4, "C");
        Point point23 = new Point(23, 24, 4, "C");
        Point point24 = new Point(24, 13, 4, "C");
        Point point25 = new Point(25, 17, 4, "C");
        Point point26 = new Point(26, 20, 4, "C");
        Point point27 = new Point(27, 15.5, 4, "C");
        Point point28 = new Point(28, 23, 3, "C");
        Point point29 = new Point(29, 32, 4, "C");

        Point point30 = new Point(30, 6, 5, "D");
        Point point31 = new Point(31, 6.5, 5, "D");
        Point point32 = new Point(32, 5.5, 5, "D");
        Point point33 = new Point(33, 7.3, 6, "D");
        Point point34 = new Point(34, 5.8, 6, "D");
        Point point35 = new Point(35, 4, 5, "D");

        ArrayList<Point> dataList = new ArrayList<Point>();
        dataList.add(point1);
        dataList.add(point2);
        dataList.add(point3);
        dataList.add(point4);
        dataList.add(point5);
        dataList.add(point6);
        dataList.add(point7);
        dataList.add(point8);
        dataList.add(point9);
        dataList.add(point10);
        dataList.add(point11);
        dataList.add(point12);
        dataList.add(point13);
        dataList.add(point14);
        dataList.add(point15);
        dataList.add(point16);
        dataList.add(point17);
        dataList.add(point18);
        dataList.add(point19);
        dataList.add(point20);
        dataList.add(point21);
        dataList.add(point22);
        dataList.add(point23);
        dataList.add(point24);
        dataList.add(point25);
        dataList.add(point26);
        dataList.add(point27);
        dataList.add(point28);
        dataList.add(point29);
        dataList.add(point30);
        dataList.add(point31);
        dataList.add(point32);
        dataList.add(point33);
        dataList.add(point34);
        dataList.add(point35);

        return dataList;
    }
}
