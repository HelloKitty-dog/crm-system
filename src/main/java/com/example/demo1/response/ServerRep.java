package com.example.demo1.response;


import com.example.demo1.enums.RepEnums;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServerRep<T> {

    private int code;

    private String msg;

    private T data;

    public static <T> ServerRep<T> SuccessDO(T data){
        ServerRep serverRep = new ServerRep(RepEnums.SUCCESS.getCode(),RepEnums.SUCCESS.getMsg(),data);
        return serverRep;
    }

    public static <T> ServerRep<T> FailureDO(T data){
        ServerRep serverRep = new ServerRep(RepEnums.FAILURE.getCode(),RepEnums.FAILURE.getMsg(),data);
        return serverRep;
    }

}
