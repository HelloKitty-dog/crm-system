package com.example.demo1.service;

import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Product;

import java.util.List;

public interface ProductService {
    //更新（修改）用户信息
    String update(Product product);

    //增加用户信息
    String insert(Product product);

    //根据id删除对象，返回成功或失败即可
    String deleteById(int id);

    //根据id返回对象，可以作为用户的查找
    Product queryById(int id);

    List<Product> findAll();

}
