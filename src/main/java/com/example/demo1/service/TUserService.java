package com.example.demo1.service;
import com.example.demo1.domain.Contract;
import com.example.demo1.vo.UserVO;
import com.example.demo1.domain.TUser;

import java.util.List;

public interface TUserService {
//更新（修改）用户信息
    String update(TUser user);

//增加用户信息
   String insert(TUser user);

 //根据id删除对象，返回成功或失败即可
   String deleteById(int id);

//根据id返回对象，可以作为用户的查找
    TUser queryById(int id);

    List<TUser> findAll();

}
