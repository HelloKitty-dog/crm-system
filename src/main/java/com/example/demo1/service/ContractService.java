package com.example.demo1.service;
import com.example.demo1.domain.Contract;
import java.util.List;

public interface ContractService {
    //更新（修改）用户信息
    String update(Contract contract);

    //增加用户信息
    String insert(Contract contract);

    //根据id删除对象，返回成功或失败即可
    String deleteById(int id);

    //根据id返回对象，可以作为用户的查找
    Contract queryById(int id);
    //查找全部信息
    List<Contract> findAll();
//    搜索全部信息
    List<Contract> searchAll(String search);


}
