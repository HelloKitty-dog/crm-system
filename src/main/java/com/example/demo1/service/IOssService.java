package com.example.demo1.service;

import com.example.demo1.response.ServerRep;
import org.springframework.web.multipart.MultipartFile;

public interface IOssService {


     ServerRep uploadFile(MultipartFile file);
}
