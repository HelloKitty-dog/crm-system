package com.example.demo1.service;

import com.example.demo1.predicted.Point;

import java.util.List;

public interface PointService {
    /**
     * 获取已知点
     */
    List<Point> allPoints();
}
