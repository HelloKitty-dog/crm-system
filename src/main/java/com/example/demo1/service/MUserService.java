package com.example.demo1.service;
import com.example.demo1.domain.MUser;

public interface MUserService {
    //更新（修改）用户信息
    String update(MUser mUser);

    //增加用户信息
    String insert(MUser mUser);

    //根据id删除对象，返回成功或失败即可
    String deleteById(int id);

    //根据id返回对象，可以作为用户的查找
    MUser queryById(int id);

    boolean checkLogin(String userName, String password);

}
