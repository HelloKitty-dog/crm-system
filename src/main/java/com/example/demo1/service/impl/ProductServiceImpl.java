package com.example.demo1.service.impl;

import com.example.demo1.dao.PrincipalMapper;
import com.example.demo1.dao.ProductMapper;
import com.example.demo1.domain.Product;
import com.example.demo1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public String update(Product product) {
        int result = productMapper.updateByPrimaryKeySelective(product);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(Product product) {
        int count = productMapper.insertSelective(product);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = productMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public Product queryById(int id) {
        Product product = productMapper.selectByPrimaryKey(id);
        return product;
    }

    @Override
    public List<Product> findAll() {
        return productMapper.selectAll();
    }
}
