package com.example.demo1.service.impl;

import com.example.demo1.dao.MUserMapper;
import com.example.demo1.domain.MUser;
import com.example.demo1.service.MUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MUserServiceImpl implements MUserService {


    @Autowired
    private MUserMapper mUserMapper;

    @Override
    public String update(MUser mUser) {
        int result = mUserMapper.updateByPrimaryKeySelective(mUser);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(MUser mUser) {
        int count = mUserMapper.insertSelective(mUser);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = mUserMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public MUser queryById(int id) {
        MUser mUser = mUserMapper.selectByPrimaryKey(id);
        return mUser;
    }

    @Override
    public boolean checkLogin(String userName, String password) {
        MUser user = mUserMapper.selectByName(userName);
        int status=user.getStatus();
        String name;
        if(status==1){
            name="管理员";
        }else{
            name="超级管理员";
        }
        //2、判断用户状态
        if (userName==null)
        {
            System.out.println("用户不存在：\n");
            //throw new Exception("该用户不存在!");
            return false;
        }
        else
        {
            //3.判断密码
            if (password==null)
            {
                //throw new Exception("密码为空!");
                System.out.println("密码为空！\n");
                return false;
            }
            else
            {
                //4、验证密码是否正确
                if (!(user.getPassword()).equals(password))
                {
                    // throw new Exception("密码错误!");
                    System.out.println("密码错误！");
                    return false;
                }
                else
                {
                    System.out.println("密码匹配成功");
                    System.out.println("欢迎"+name+"登录");
                    return true;
                }
            }
        }

    }


}
