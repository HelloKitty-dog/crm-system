package com.example.demo1.service.impl;

import com.example.demo1.dao.ContractMapper;
import com.example.demo1.dao.PrincipalMapper;
import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Principal;
import com.example.demo1.service.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrincipalServiceImpl implements PrincipalService {

    @Autowired
    private PrincipalMapper principalMapper;
    @Override
    public String update(Principal principal) {
        int result = principalMapper.updateByPrimaryKeySelective(principal);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(Principal principal) {
        int count = principalMapper.insertSelective(principal);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = principalMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public Principal queryById(int id) {
        Principal principal = principalMapper.selectByPrimaryKey(id);
        return principal;
    }

    @Override
    public List<Principal> findAll() {
        return principalMapper.selectAll();    }
}
