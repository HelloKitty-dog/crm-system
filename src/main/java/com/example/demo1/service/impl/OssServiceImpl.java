package com.example.demo1.service.impl;

import com.example.demo1.response.ServerRep;
import com.example.demo1.service.IOssService;
import com.example.demo1.utils.OSSUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.function.ServerResponse;

import java.io.*;

@Slf4j
@Service
public class OssServiceImpl implements IOssService {

    @Override
    public ServerRep uploadFile(MultipartFile multipartFile) {
        File file = multipartFileToFile(multipartFile);
        String fileUri = OSSUtil.uploadFile(file);
        delteTempFile(file);  //上传完成之后删除临时文件
        return ServerRep.SuccessDO(fileUri);
    }

    /**
     * fransfor multipartfile to file
     * @param file
     * @return
     */
    private File multipartFileToFile(MultipartFile file){

        File targetFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            targetFile = null;
        } else {
            InputStream ins = null;
            try {
                ins = file.getInputStream();
                targetFile = new File(file.getOriginalFilename());
                inputStreamToFile(ins, targetFile);
                ins.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return targetFile;
    }

    /**
     * 删除
     * @param file
     */
    public static void delteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }
    }


    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
