package com.example.demo1.service.impl;

import com.example.demo1.dao.TUserMapper;
import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Product;
import com.example.demo1.domain.TUser;
import com.example.demo1.service.TUserService;
import com.example.demo1.vo.UserVO;
import com.example.demo1.enums.Userstatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TUserServiceImpl implements TUserService {

    @Autowired
    private TUserMapper tUserMapper;


//修改功能的实现
    @Override
    public String update(TUser user) {
        int result = tUserMapper.updateByPrimaryKeySelective(user);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    //增加功能的实现
    @Override
    public String insert(TUser user) {
        int count = tUserMapper.insertSelective(user);
        if(count == 1){
            return "success";
      }
        return "failure";

    }


//删除功能的实现
    @Override
    public String deleteById(int id) {
        int count = tUserMapper.deleteByPrimaryKey(id);
        if(count==1){
            return "success";
        }
        return "failure";
    }

    @Override
    public TUser queryById(int id) {
        TUser tUser = tUserMapper.selectByPrimaryKey(id);
        return tUser;
    }
//查找功能的实现，顺带把user中的statue用vo转换成了另一种形式

    @Override
    public List<TUser> findAll() {
        return tUserMapper.selectAll();
    }
}
