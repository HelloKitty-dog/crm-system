package com.example.demo1.service.impl;

import com.example.demo1.dao.ContractMapper;
import com.example.demo1.domain.Contract;
import com.example.demo1.service.ContractService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractMapper contractMapper;

    @Override
    public String update(Contract contract) {
        int result = contractMapper.updateByPrimaryKeySelective(contract);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(Contract contract) {
        int count = contractMapper.insertSelective(contract);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = contractMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public Contract queryById(int id) {
        Contract contract = contractMapper.selectByPrimaryKey(id);
        return contract;
    }

    @Override
    public List<Contract> findAll() {
        return contractMapper.selectAll();
    }

    @Override
    public List<Contract> searchAll(String search) {
        return contractMapper.searchAll(search);
    }

}