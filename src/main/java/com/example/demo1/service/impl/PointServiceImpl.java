package com.example.demo1.service.impl;

import com.example.demo1.dao.PointMapper;
import com.example.demo1.predicted.Point;
import com.example.demo1.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PointServiceImpl implements PointService {
    @Autowired
    private PointMapper pointMapper;

    @Override
    public List<Point> allPoints() {
        return pointMapper.allPoints();
    }
}
