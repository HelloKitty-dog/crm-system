package com.example.demo1.service.impl;

import com.example.demo1.dao.ContractMapper;
import com.example.demo1.dao.HighSeaMapper;
import com.example.demo1.domain.Contract;
import com.example.demo1.domain.HighSea;
import com.example.demo1.service.HighSeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class HighSeaServiceImpl implements HighSeaService {

    @Autowired
    private HighSeaMapper highSeaMapper;

    @Override
    public String update(HighSea highSea) {
        int result = highSeaMapper.updateByPrimaryKeySelective(highSea);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(HighSea highSea) {
        int count = highSeaMapper.insertSelective(highSea);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = highSeaMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public HighSea queryById(int id) {
        HighSea highSea = highSeaMapper.selectByPrimaryKey(id);
        return highSea;
    }

    @Override
    public List<HighSea> findAll() {
        return highSeaMapper.selectAll();
    }
}
