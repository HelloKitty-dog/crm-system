package com.example.demo1.service.impl;

import com.example.demo1.dao.FollowMapper;
import com.example.demo1.domain.Follow;
import com.example.demo1.service.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowServiceImpl implements FollowService {
    @Autowired
    private FollowMapper followMapper;


    @Override
    public String update(Follow follow) {
        int result = followMapper.updateByPrimaryKeySelective(follow);
        if (result >= 1) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public String insert(Follow follow) {
        int count = followMapper.insertSelective(follow);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public String deleteById(int id) {
        int count = followMapper.deleteByPrimaryKey(id);
        if (count == 1) {
            return "success";
        }
        return "failure";
    }

    @Override
    public Follow queryById(int id) {
        Follow follow = followMapper.selectByPrimaryKey(id);
        return follow;
    }

    @Override
    public List<Follow> findAll(){
            return followMapper.selectAll();
    }
}
