package com.example.demo1.controller;

import com.example.demo1.domain.HighSea;
import com.example.demo1.response.ServerRep;
import com.example.demo1.service.HighSeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/highsea")
public class HighSeaController {
    @Autowired
    private HighSeaService highSeaService;

    //这个是查看对象的url和方法，返回一个对象，主要原因是iUserService.queryById(id)
    @GetMapping(value = "/info")
    public ServerRep detail(@RequestParam int id){
        return ServerRep.SuccessDO(highSeaService.queryById(id));
    }
    //这个是实现删除对象的url和方法
    @GetMapping(value = "/delete")
    public  ServerRep delete(@RequestParam int id){
        return ServerRep.SuccessDO((highSeaService.deleteById(id)));
    }
    //这个是实现新增的url和方法
    @PostMapping(value = "/insert")
    public  ServerRep insert(@RequestBody HighSea highSea){return ServerRep.SuccessDO(highSeaService.insert(highSea)); }
    //这个是实现修改功能
    @PutMapping(value = "/update")
    public ServerRep update(@RequestBody HighSea highSea) { return ServerRep.SuccessDO(highSeaService.update(highSea)); }
    //这个是查找全部信息
    @GetMapping(value = "/all")
    public ServerRep findAll(){ return ServerRep.SuccessDO(highSeaService.findAll()); }
}
