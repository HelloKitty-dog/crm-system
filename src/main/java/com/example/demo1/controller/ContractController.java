package com.example.demo1.controller;

import com.example.demo1.domain.Contract;
import com.example.demo1.params.BaseQueryParams;
import com.example.demo1.response.ServerRep;
import com.example.demo1.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/contract")
public class ContractController {

    @Autowired
    private ContractService contractService;

    //这个是查看对象的url和方法，返回一个对象，主要原因是iUserService.queryById(id)
    @GetMapping(value = "/info")
    public ServerRep detail(@RequestParam int id){
        return ServerRep.SuccessDO(contractService.queryById(id));
    }
    //这个是实现删除对象的url和方法
    @GetMapping(value = "/delete")
    public  ServerRep delete(@RequestParam int id){
        return ServerRep.SuccessDO((contractService.deleteById(id)));
    }
    //这个是实现新增的url和方法
    @PostMapping(value = "/insert")
    public  ServerRep insert(@RequestBody Contract contract){return ServerRep.SuccessDO(contractService.insert(contract)); }
    //这个是实现修改功能
    @PutMapping(value = "/update")
    public ServerRep update(@RequestBody  Contract contract) { return ServerRep.SuccessDO(contractService.update(contract)); }
    //这个是查找全部信息
    @GetMapping(value = "/all")
    public ServerRep findAll(){ return ServerRep.SuccessDO(contractService.findAll()); }
    //搜索
    @PostMapping(value = "/search")
    public ServerRep searchAll(@RequestBody BaseQueryParams params){
        return ServerRep.SuccessDO(contractService.searchAll(params.getSearch()));
    }

    //搜索
    @PostMapping(value = "/search1")
    public ServerRep searchAll(@RequestParam String search){
        return ServerRep.SuccessDO(contractService.searchAll(search));
    }

    //搜索
    @PostMapping(value = "/search2/{search}")
    public ServerRep searchAll1(@PathVariable String  search){
        return ServerRep.SuccessDO(contractService.searchAll(search));
    }

}
