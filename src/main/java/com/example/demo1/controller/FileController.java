package com.example.demo1.controller;


import com.example.demo1.response.ServerRep;
import com.example.demo1.service.IOssService;
import com.example.demo1.utils.OSSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@RequestMapping(value = "/v1/file")
public class FileController {


    @Autowired
    private IOssService iOssService;

    //这个是查看对象的url和方法，返回一个对象，主要原因是iUserService.queryById(id)
    @PostMapping(value = "/upload")
    public ServerRep upload(@RequestParam("file")MultipartFile file){
        return ServerRep.SuccessDO(iOssService.uploadFile(file));
    }

}
