package com.example.demo1.controller;

import com.example.demo1.domain.Contract;
import com.example.demo1.domain.Principal;
import com.example.demo1.response.ServerRep;
import com.example.demo1.service.ContractService;
import com.example.demo1.service.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/principal")
public class PrincipalController {
    @Autowired
    private PrincipalService principalService;

    //这个是查看对象的url和方法，返回一个对象，主要原因是iUserService.queryById(id)
    @GetMapping(value = "/info")
    public ServerRep detail(@RequestParam int id){
        return ServerRep.SuccessDO(principalService.queryById(id));
    }
    //这个是实现删除对象的url和方法
    @GetMapping(value = "/delete")
    public  ServerRep delete(@RequestParam int id){
        return ServerRep.SuccessDO((principalService.deleteById(id)));
    }
    //这个是实现新增的url和方法
    @PostMapping(value = "/insert")
    public  ServerRep insert(@RequestBody Principal principal){return ServerRep.SuccessDO(principalService.insert(principal)); }
    //这个是实现修改功能
    @PutMapping(value = "/update")
    public ServerRep update(@RequestBody  Principal principal) { return ServerRep.SuccessDO(principalService.update(principal)); }
    @GetMapping(value = "/all")
    public ServerRep findAll(){ return ServerRep.SuccessDO(principalService.findAll()); }
}
