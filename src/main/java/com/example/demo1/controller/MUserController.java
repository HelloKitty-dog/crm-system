package com.example.demo1.controller;

import com.example.demo1.domain.MUser;
import com.example.demo1.response.ServerRep;
import com.example.demo1.service.MUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/Muser")
public class MUserController {
    @Autowired
    private MUserService mUserService;

    //这个是查看对象的url和方法，返回一个对象，主要原因是iUserService.queryById(id)
    @GetMapping(value = "/info")
    public ServerRep detail(@RequestParam int id) {
        return ServerRep.SuccessDO(mUserService.queryById(id));
    }

    //这个是实现删除对象的url和方法
    @GetMapping(value = "/delete")
    public ServerRep delete(@RequestParam int id) {
        return ServerRep.SuccessDO((mUserService.deleteById(id)));
    }

    //这个是实现新增的url和方法
    @PostMapping(value = "/insert")
    public ServerRep insert(@RequestBody MUser mUser) {
        return ServerRep.SuccessDO(mUserService.insert(mUser));
    }

    //这个是实现修改功能
    @PutMapping(value = "/update")
    public ServerRep update(@RequestBody MUser mUser) {
        return ServerRep.SuccessDO(mUserService.update(mUser));
    }

    @PostMapping(value = "/login")
    public boolean login(@RequestBody MUser mUser) {
        return mUserService.checkLogin(mUser.getUsername(), mUser.getPassword());
    }

}