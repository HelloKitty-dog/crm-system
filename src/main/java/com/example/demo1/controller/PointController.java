package com.example.demo1.controller;

import com.example.demo1.predicted.CompareClass;
import com.example.demo1.predicted.Distance;
import com.example.demo1.predicted.Point;
import com.example.demo1.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class PointController {

    @Autowired
    private PointService pointService;

    @RequestMapping(value = "/point", method = RequestMethod.POST)
    public String getDemoPoint(@RequestBody Point unknownPoint) {

        // 获取所有点
        List<Point> dataList = pointService.allPoints();
        // 前端来的未知点
        Point x = unknownPoint;

        // 计算所有已知点到未知点的欧式距离，并根据距离对所有已知点排序
        CompareClass compare = new CompareClass();
        Set<Distance> distanceSet = new TreeSet<Distance>(compare);
        for (Point point : dataList) {
            distanceSet.add(new Distance(point.getId(), x.getId(), oudistance(point,
                    x)));
        }
        // 选取最近的k个点
        double k = 5;

        /**
         * 计算k个点所在分类出现的频率
         */
        // 计算每个分类所包含的点的个数
        List<Distance> distanceList= new ArrayList<Distance>(distanceSet);
        Map<String, Integer> map = getNumberOfType(distanceList, dataList, k);

        // 计算频率
        Map<String, Double> p = computeP(map, k);

        x.setProduct(maxP(p));

        return x.getProduct();

        //return userService.addUser(user);
    }

    // 欧式距离计算
    public static double oudistance(Point point1, Point point2) {
        double temp = Math.pow(point1.getIncome() - point2.getIncome(), 2)
                + Math.pow(point1.getPersonnum() - point2.getPersonnum(), 2);
        return Math.sqrt(temp);
    }

    // 找出最大频率
    public static String maxP(Map<String, Double> map) {
        String key = null;
        double value = 0.0;
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (entry.getValue() > value) {
                key = entry.getKey();
                value = entry.getValue();
            }
        }
        return key;
    }

    // 计算频率
    public static Map<String, Double> computeP(Map<String, Integer> map,
                                               double k) {
        Map<String, Double> p = new HashMap<String, Double>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            p.put(entry.getKey(), entry.getValue() / k);
        }
        return p;
    }

    // 计算每个分类包含的点的个数
    public static Map<String, Integer> getNumberOfType(
            List<Distance> listDistance, List<Point> listPoint, double k) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        int i = 0;
        System.out.println("选取的k个点，由近及远依次为：");
        for (Distance distance : listDistance) {
            System.out.println("id为" + distance.getId() + ",距离为："
                    + distance.getDisatance());
            long id = distance.getId();
            // 通过id找到所属类型,并存储到HashMap中
            for (Point point : listPoint) {
                if (point.getId() == id) {
                    if (map.get(point.getProduct()) != null)
                        map.put(point.getProduct(), map.get(point.getProduct()) + 1);
                    else {
                        map.put(point.getProduct(), 1);
                    }
                }
            }
            i++;
            if (i >= k)
                break;
        }
        return map;
    }

}