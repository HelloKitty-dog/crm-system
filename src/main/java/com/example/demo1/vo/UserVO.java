package com.example.demo1.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {

    private Integer id;

    private String userName;

    private Integer age;

    private String statusDesc;

    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date createAt;

    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date updateAt;

    private String tel;

    private String address;

    private String userNo;

    private String userFrom;

    private String describes;

    private String intentions;

    private String business;

}
