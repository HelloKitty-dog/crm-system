package com.example.demo1.domain;

import java.util.Date;

public class Predict {

    private Integer id;

    private String pname;

    private Integer age;

    private Double income;

    private Integer personum;

    private Double consumption;

    private String address;

    private String product;

    private String source;

    private String business;

    private Date signdate;

    public Predict(Integer id, String pname, Integer age, Double income, Integer personum, Double consumption, String address, String product, String source, String business, Date signdate) {
        this.id = id;
        this.pname = pname;
        this.age = age;
        this.income = income;
        this.personum = personum;
        this.consumption = consumption;
        this.address = address;
        this.product = product;
        this.source = source;
        this.business = business;
        this.signdate = signdate;
    }

    public Predict() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname == null ? null : pname.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public Integer getPersonum() {
        return personum;
    }

    public void setPersonum(Integer personum) {
        this.personum = personum;
    }

    public Double getConsumption() {
        return consumption;
    }

    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business == null ? null : business.trim();
    }

    public Date getSigndate() {
        return signdate;
    }

    public void setSigndate(Date signdate) {
        this.signdate = signdate;
    }
}