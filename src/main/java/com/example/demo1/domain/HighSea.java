package com.example.demo1.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class HighSea {

    private Integer id;

    private String hname;

    private String sex;

    private Integer age;

    private String describes;

    private String fromwhere;

    private String tel;

    private String intention;

    private String principal;

    private Integer isover;

    private String address;

    private String business;
    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date createAt;

    public HighSea(Integer id, String hname, String sex, Integer age, String describes, String fromwhere, String tel, String intention, String principal, Integer isover, String address, String business, Date createAt) {
        this.id = id;
        this.hname = hname;
        this.sex = sex;
        this.age = age;
        this.describes = describes;
        this.fromwhere = fromwhere;
        this.tel = tel;
        this.intention = intention;
        this.principal = principal;
        this.isover = isover;
        this.address = address;
        this.business = business;
        this.createAt = createAt;
    }

    public HighSea() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHname() {
        return hname;
    }

    public void setHname(String hname) {
        this.hname = hname == null ? null : hname.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes == null ? null : describes.trim();
    }

    public String getFromwhere() {
        return fromwhere;
    }

    public void setFromwhere(String fromwhere) {
        this.fromwhere = fromwhere == null ? null : fromwhere.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getIntention() {
        return intention;
    }

    public void setIntention(String intention) {
        this.intention = intention == null ? null : intention.trim();
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    public Integer getIsover() {
        return isover;
    }

    public void setIsover(Integer isover) {
        this.isover = isover;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business == null ? null : business.trim();
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}