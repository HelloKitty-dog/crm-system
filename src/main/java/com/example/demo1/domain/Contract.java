package com.example.demo1.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Contract {

    private Integer id;

    private String hno;

    private String principal;

    private Double money;

    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date signdate;

    private String describes;

    private String clause;

    public Contract(Integer id, String hno, String principal, Double money, Date signdate, String describes, String clause) {
        this.id = id;
        this.hno = hno;
        this.principal = principal;
        this.money = money;
        this.signdate = signdate;
        this.describes = describes;
        this.clause = clause;
    }

    public Contract() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHno() {
        return hno;
    }

    public void setHno(String hno) {
        this.hno = hno == null ? null : hno.trim();
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Date getSigndate() {
        return signdate;
    }

    public void setSigndate(Date signdate) {
        this.signdate = signdate;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes == null ? null : describes.trim();
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause == null ? null : clause.trim();
    }
}