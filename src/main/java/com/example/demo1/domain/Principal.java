package com.example.demo1.domain;

public class Principal {

    private Integer id;

    private String fname;

    private String fno;

    public Principal(Integer id, String fname, String fno) {
        this.id = id;
        this.fname = fname;
        this.fno = fno;
    }

    public Principal() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname == null ? null : fname.trim();
    }

    public String getFno() {
        return fno;
    }

    public void setFno(String fno) {
        this.fno = fno == null ? null : fno.trim();
    }
}