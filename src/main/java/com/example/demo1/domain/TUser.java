package com.example.demo1.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;

import java.util.Date;
@Builder(toBuilder = true)
public class TUser {

    private Integer id;

    private String userName;

    private Integer age;

    private Byte status;
    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date createAt;
    @JsonFormat(pattern = "yyyy-MM-DD HH:mm:ss")
    private Date updateAt;

    private String tel;

    private String address;

    private String userNo;

    private String userFrom;

    private String describes;

    private String intentions;

    private String business;

    public TUser(Integer id, String userName, Integer age, Byte status, Date createAt, Date updateAt, String tel, String address, String userNo, String userFrom, String describes, String intentions, String business) {
        this.id = id;
        this.userName = userName;
        this.age = age;
        this.status = status;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.tel = tel;
        this.address = address;
        this.userNo = userNo;
        this.userFrom = userFrom;
        this.describes = describes;
        this.intentions = intentions;
        this.business = business;
    }

    public TUser() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo == null ? null : userNo.trim();
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom == null ? null : userFrom.trim();
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes == null ? null : describes.trim();
    }

    public String getIntentions() {
        return intentions;
    }

    public void setIntentions(String intentions) {
        this.intentions = intentions == null ? null : intentions.trim();
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business == null ? null : business.trim();
    }
}