package com.example.demo1.domain;

public class Product {

    private Integer id;

    private String cno;

    private String cname;

    private Integer count;

    private String unit;

    private String describes;


    public Product(Integer id, String cno, String cname, Integer count, String unit, String describes) {
        this.id = id;
        this.cno = cno;
        this.cname = cname;
        this.count = count;
        this.unit = unit;
        this.describes = describes;
    }

    public Product() {
        super();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes == null ? null : describes.trim();
    }
}