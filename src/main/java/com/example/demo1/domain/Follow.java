package com.example.demo1.domain;

public class Follow {

    private Integer id;

    private String cusname;

    private String cusno;

    private String hno;

    private String principal;

    private Integer status;

    public Follow(Integer id, String cusname, String cusno, String hno, String principal, Integer status) {
        this.id = id;
        this.cusname = cusname;
        this.cusno = cusno;
        this.hno = hno;
        this.principal = principal;
        this.status = status;
    }

    public Follow() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCusname() {
        return cusname;
    }

    public void setCusname(String cusname) {
        this.cusname = cusname == null ? null : cusname.trim();
    }

    public String getCusno() {
        return cusno;
    }

    public void setCusno(String cusno) {
        this.cusno = cusno == null ? null : cusno.trim();
    }

    public String getHno() {
        return hno;
    }

    public void setHno(String hno) {
        this.hno = hno == null ? null : hno.trim();
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}